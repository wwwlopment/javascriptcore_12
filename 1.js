class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

var person1 = new Person('Вася', 21);
var person2 = new Person('Таня', 22);
var person3 = new Person('Оля', 19);
var person4 = new Person('Петя', 24);
var person5 = new Person('Юля', 20);

var map = new Map();
map.set(1, person1);
map.set(2, person2);
map.set(3, person3);
map.set(4, person4);
map.set(5, person5);

for(let entry of map.values()) {
  console.log(entry);
}
